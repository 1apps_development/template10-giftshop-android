package com.workdo.gifts.ui.activity

import android.app.AlertDialog
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.workdo.gifts.R
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActMainBinding
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.ui.fragment.*
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils

class ActMain : BaseActivity() {
    private lateinit var _binding: ActMainBinding
    var pos = ""
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding= ActMainBinding.inflate(layoutInflater)
        init()
    }


    private fun init(){
        val name = SharePreference.getStringPref(this@ActMain, SharePreference.userName)
        Log.e("name", name.toString())
        pos = intent.getStringExtra("pos").toString()
        Log.e("Pos", pos)
        if(SharePreference.getStringPref(this@ActMain,SharePreference.cartCount).isNullOrEmpty())
        {
            SharePreference.setStringPref(this@ActMain,SharePreference.cartCount,"0")
        }

        when (pos) {
            "1" -> {
                setCurrentFragment(FragHome())
                _binding.bottomNavigation.selectedItemId = R.id.ivBestSellers
            }
            "2" -> {
                setCurrentFragment(FragProducts())
                _binding.bottomNavigation.selectedItemId = R.id.ivBirthday
            }
            "3" -> {
                setCurrentFragment(FragCategories())
                _binding.bottomNavigation.selectedItemId = R.id.ivDashBoard
            }
            "4" -> {
                setCurrentFragment(FragWishList())
                _binding.bottomNavigation.selectedItemId = R.id.ivOccasion
            }
            "5" -> {
                setCurrentFragment(FragSettings())
                _binding.bottomNavigation.selectedItemId = R.id.ivAnniversary
            }
            else -> {
                mainFragment(FragHome())
            }
        }


        bottomSheetItemNavigation()
    }


    private fun bottomSheetItemNavigation() {
        _binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)

            when (item.itemId) {
                R.id.ivBestSellers -> {
                    if (fragment !is FragHome) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragHome())
                    }
                    true
                }
                R.id.ivBirthday -> {
                    if (fragment !is FragProducts) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragProducts())
                    }
                    true
                }
                R.id.ivDashBoard -> {

                    if (fragment !is FragCategories) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragCategories())
                        SharePreference.setStringPref(this@ActMain,SharePreference.clickMenu,"click")
                    }else
                    {
                        fragment.showLayout()
                    }
                    true
                }
                R.id.ivOccasion -> {
                    if (Utils.isLogin(this@ActMain)) {
                        if (fragment !is FragWishList) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragWishList())
                        }
                    } else {
                        Utils.openWelcomeScreen(this@ActMain)
                    }

                    true
                }
                R.id.ivAnniversary -> {
                    if (Utils.isLogin(this@ActMain)) {
                        if (fragment !is FragSettings) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragSettings())
                        }
                    } else {
                        Utils.openWelcomeScreen(this@ActMain)
                    }
                    true
                }
                else -> {
                    false
                }
            }
        }
    }

     fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragContainer, fragment)
            commit()
        }

    private fun mainFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            add(R.id.fragContainer, fragment)
            commit()
        }


    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        } else {
            mExitDialog()
        }
    }

    private fun mExitDialog() {
        val builder = AlertDialog.Builder(this@ActMain)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.exit)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
            ActivityCompat.finishAfterTransition(this@ActMain)
            ActivityCompat.finishAffinity(this@ActMain);
            finish()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            dialogInterface.dismiss()
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

}