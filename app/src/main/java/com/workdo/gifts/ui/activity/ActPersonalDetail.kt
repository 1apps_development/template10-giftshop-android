package com.workdo.gifts.ui.activity

import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActPersonalDetailBinding
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch

class ActPersonalDetail : BaseActivity() {
    private lateinit var _binding: ActPersonalDetailBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActPersonalDetailBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.edFirstName.setText(
            SharePreference.getStringPref(this@ActPersonalDetail, SharePreference.userFirstName)
                .toString()
        )
        _binding.edLastName.setText(SharePreference.getStringPref(this@ActPersonalDetail,
            SharePreference.userLastName).toString())
        _binding.edEmail.setText(SharePreference.getStringPref(this@ActPersonalDetail,
            SharePreference.userEmail).toString())
        _binding.edPhone.setText(SharePreference.getStringPref(this@ActPersonalDetail,
            SharePreference.userMobile).toString())

        _binding.btnEditProfile.setOnClickListener {
            if (_binding.edFirstName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_f_name))
            } else if (_binding.edLastName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_l_name))
            } else if (_binding.edEmail.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_email))
            } else if (!Utils.isValidEmail(_binding.edEmail.text.toString())) {
                Utils.errorAlert(this, resources.getString(R.string.validation_valid_email))
            } else if (_binding.edPhone.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_phone_number))
            } else {
                val editprofile = HashMap<String, String>()
                editprofile["user_id"] = SharePreference.getStringPref(this@ActPersonalDetail, SharePreference.userId).toString()
                editprofile["first_name"] = _binding.edFirstName.text.toString()
                editprofile["last_name"] = _binding.edLastName.text.toString()
                editprofile["email"] = _binding.edEmail.text.toString()
                editprofile["telephone"] = _binding.edPhone.text.toString()
                editprofile["theme_id"] = resources.getString(R.string.theme_id)

                callEditProfile(editprofile)
            }
        }
    }

    private fun callEditProfile(editprofile: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActPersonalDetail)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActPersonalDetail).setProfileUpdate(editprofile)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val editProfileResponse = response.body.data?.data
                    when (response.body.status) {
                        1 -> {
                            Log.e("UserData", editProfileResponse.toString())
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userName,
                                editProfileResponse?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userFirstName,
                                editProfileResponse?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userLastName,
                                editProfileResponse?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userEmail,
                                editProfileResponse?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userMobile,
                                editProfileResponse?.mobile.toString()
                            )
                            finish()
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActPersonalDetail,
                                response.body.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActPersonalDetail,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActPersonalDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActPersonalDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPersonalDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPersonalDetail,
                        resources.getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

}