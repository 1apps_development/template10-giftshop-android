package com.workdo.gifts.ui.activity

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.gifts.R
import com.workdo.gifts.adapter.AutoCompleteCityAdapter
import com.workdo.gifts.adapter.AutoCompleteCountryAdapter
import com.workdo.gifts.adapter.AutoCompleteStateAdapter
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActAddressBinding
import com.workdo.gifts.model.CityListData
import com.workdo.gifts.model.CountryDataItem
import com.workdo.gifts.model.StateListData
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.*
import kotlinx.coroutines.launch

class ActAddress : BaseActivity(),
    OnItemClickListenerCountry, OnItemClickListenerState, OnItemClickListenerCity {
    private lateinit var _binding: ActAddressBinding
    private lateinit var countryAdapterauto: AutoCompleteCountryAdapter
    private lateinit var countryAdapterautoState: AutoCompleteStateAdapter
    private lateinit var countryAdapterautoCity: AutoCompleteCityAdapter

    private var countryList = ArrayList<CountryDataItem>()
    private var stateList = ArrayList<StateListData>()
    private var cityList = ArrayList<CityListData>()
    val addressTypeArray = arrayListOf<String>()
    val addressTypeArrayID = arrayListOf<String>()
    val addressTypeArrayState = arrayListOf<String>()
    var countryID = ""
    var cityID = ""
    var stateId = ""
    var checkDef = "0"
    var type = ""
    var addresstype = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActAddressBinding.inflate(layoutInflater)
        init()
        onTouchListener()
    }

    private fun onTouchListener()
    {
        _binding.autoCompleteCountry.onFocusChangeListener =
            View.OnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    _binding.autoCompleteCountry.showDropDown()
                }

            }
        _binding.autoCompleteCity.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                _binding.autoCompleteCity.showDropDown()
            }

        }
        _binding.autoCompleteState.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                _binding.autoCompleteState.showDropDown()
            }

        }

    }

    private fun init(){
        addresstype = intent.getStringExtra("AddAddress").toString()
        _binding.ivBack.setOnClickListener {
            if (addresstype == "AddAddress") {
                val intent = Intent(this@ActAddress, ActBillingDetails::class.java)
                finish()
                startActivity(intent)
            } else {
                finish()
            }
        }
        type = intent.getStringExtra("type").toString()
        Log.e("type", type)
        _binding.radioYes.isChecked = false
        _binding.radioNo.isChecked = true
        countryID = intent.getStringExtra("country_id").toString()
        stateId = intent.getStringExtra("state_id").toString()
        _binding.edSaveas.setText(intent.getStringExtra("title"))
        _binding.edAddressName.setText(intent.getStringExtra("address"))
        _binding.autoCompleteCity.setText(intent.getStringExtra("city"))
        _binding.edPostCode.setText(intent.getStringExtra("postcode"))
        if (type == "1") {
            _binding.autoCompleteCountry.setText(intent.getStringExtra("country_name"))
            _binding.autoCompleteState.setText(intent.getStringExtra("state_name"))
        }
        _binding.radioYes.isChecked = intent.getStringExtra("default_address").toString() == "1"

        _binding.radioYes.setOnClickListener {
            checkDef = "1"

        }
        _binding.radioNo.setOnClickListener {
            checkDef = "0"
        }

        _binding.btnSaveChanges.setOnClickListener {
            when {
                _binding.edSaveas.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.please_enter_save_address)
                    )
                }

                _binding.edAddressName.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.please_enter_address)
                    )
                }
                _binding.autoCompleteCity.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.please_enter_city)
                    )
                }
                _binding.edPostCode.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.please_enter_postcode)
                    )
                }
                _binding.edPostCode.text.length != 6  -> {
                    Utils.errorAlert(this@ActAddress,resources.getString(R.string.please_enter_valid_postcode))
                }
                _binding.autoCompleteCountry.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.please_enter_country)
                    )
                }
                _binding.autoCompleteState.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.please_enter_state)
                    )
                }
                else -> {

                    if (type == "1") {
                        val editAddressMap = HashMap<String, String>()
                        editAddressMap["address_id"] =
                            intent.getStringExtra("addressId").toString()
                        editAddressMap["user_id"] =
                            SharePreference.getStringPref(
                                this@ActAddress,
                                SharePreference.userId
                            )
                                .toString()
                        editAddressMap["title"] = _binding.edSaveas.text.toString()
                        editAddressMap["address"] = _binding.edAddressName.text.toString()
                        editAddressMap["country"] = countryID
                        editAddressMap["state"] = stateId
                        editAddressMap["city"] = _binding.autoCompleteCity.text.toString()
                        editAddressMap["postcode"] = _binding.edPostCode.text.toString()
                        editAddressMap["default_address"] = checkDef
                        editAddressMap["theme_id"] = resources.getString(R.string.theme_id)

                        callUpdateAddressApi(editAddressMap)
                    } else {
                        val addressMap = HashMap<String, String>()
                        addressMap["user_id"] =
                            SharePreference.getStringPref(
                                this@ActAddress,
                                SharePreference.userId
                            )
                                .toString()
                        addressMap["title"] = _binding.edSaveas.text.toString()
                        addressMap["address"] = _binding.edAddressName.text.toString()
                        addressMap["country"] = countryID
                        addressMap["state"] = stateId
                        addressMap["city"] = _binding.autoCompleteCity.text.toString()
                        addressMap["postcode"] = _binding.edPostCode.text.toString()
                        addressMap["default_address"] = checkDef
                        addressMap["theme_id"] = resources.getString(R.string.theme_id)

                        callAddressApi(addressMap)
                    }
                }
            }
        }

        getCountryApi()

    }

    //TODO address list api
    private fun callAddressApi(addressMap: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActAddress)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddress)
                .addAddress(addressMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (addresstype == "AddAddress") {
                                val intent =
                                    Intent(this@ActAddress, ActBillingDetails::class.java)
                                finish()
                                startActivity(intent)
                            } else {
                                finish()
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                addressResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Update address api
    private fun callUpdateAddressApi(editAddressMap: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActAddress)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddress)
                .updateAddress(editAddressMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            finish()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                addressResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO country list api
    private fun getCountryApi() {
        Utils.showLoadingProgress(this@ActAddress)
        val request=HashMap<String,String>()
        request["theme_id"]=resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddress)
                .setCountryList(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val countryResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            countryList = countryResponse.data!!
                            loadSpinnerCountry(countryList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                countryResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Country spinner
    private fun loadSpinnerCountry(countryList: ArrayList<CountryDataItem>) {
        /*for (i in 0 until countryList.size) {
            addressTypeArray.add(
                countryList[i].name.toString()
            )
        }*/

        countryAdapterauto =
            AutoCompleteCountryAdapter(
                this@ActAddress,
                countryList,
                OnItemClickListenerCountry {
                    onItemClick(it)
                    Log.e("StateId", it.id.toString())

                })

        _binding.autoCompleteCountry.threshold = 0
        _binding.autoCompleteCountry.setAdapter(countryAdapterauto)

    }

    //TODO state list api
    private fun getStateApi(countryID: String) {
        Utils.showLoadingProgress(this@ActAddress)
        val stateListMap = HashMap<String, String>()
        stateListMap["country_id"] = countryID
        stateListMap["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddress)
                .setStateList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            stateList = stateListResponse.data!!

                            loadSpinnerState(stateList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO state list api
    private fun getCityApi(stateId: String) {
        Utils.showLoadingProgress(this@ActAddress)
        val stateListMap = HashMap<String, String>()
        stateListMap["state_id"] = stateId
        stateListMap["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActAddress)
                .setCityList(stateListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            cityList = stateListResponse.data!!

                            loadCityAdapter(cityList)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActAddress,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActAddress)
                    } else {
                        Utils.errorAlert(
                            this@ActAddress,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActAddress,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loadCityAdapter(cityList: ArrayList<CityListData>) {
        /*    for (element in cityList) {
                addressTypeArraycity.add(element.name.toString())
                Log.e("CityList", addressTypeArraycity.toString())

            }

            val arrayAdapter: ArrayAdapter<CityListData> = ArrayAdapter<CityListData>(
                this@ActAddAddress, R.layout.row_item_list,
                R.id.tvItemName,
                addressTypeArraycity as List<CityListData>
            )
            _binding.autoCompleteCity.threshold = 2
            _binding.autoCompleteCity.setAdapter(arrayAdapter)*/

        countryAdapterautoCity =
            AutoCompleteCityAdapter(
                this@ActAddress,
                cityList
            ) {
                onItemClickCity(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.autoCompleteCity.threshold = 0
        _binding.autoCompleteCity.setAdapter(countryAdapterautoCity)
    }

    //TODO State list spinner
    private fun loadSpinnerState(stateList: ArrayList<StateListData>) {
        /*addressTypeArrayState.clear()
        for (i in 0 until stateList.size) {
            addressTypeArrayState.add(
                stateList[i].name.toString()
            )
        }*/

        countryAdapterautoState =
            AutoCompleteStateAdapter(
                this@ActAddress,
                stateList
            ) {
                onItemClickState(it)
                Log.e("StateId", it.id.toString())

            }

        _binding.autoCompleteState.threshold = 0
        _binding.autoCompleteState.setAdapter(countryAdapterautoState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (addresstype == "AddAddress") {
            val intent = Intent(this@ActAddress, ActBillingDetails::class.java)
            finish()
            startActivity(intent)
        } else {
            finish()
        }
    }

    override fun onItemClick(item: CountryDataItem?) {
        Log.e("StateId", item?.id.toString())
        _binding.autoCompleteState.text.clear()
        _binding.autoCompleteCity.text.clear()
        _binding.autoCompleteCountry.setText(item?.name)
        countryID=item?.id.toString()
        stateId = item?.id.toString()
        getStateApi(item?.id.toString())
    }

    override fun onItemClickState(item: StateListData?) {
        _binding.autoCompleteState.setText(item?.name)
        _binding.autoCompleteCity.text.clear()
        stateId = item?.id.toString()
        cityID = item?.id.toString()
        getCityApi(item?.id.toString())
    }

    override fun onItemClickCity(item: CityListData?) {
        _binding.autoCompleteCity.setText(item?.name)
        _binding.autoCompleteCity.dismissDropDown()
        cityID=item?.id.toString()
    }

}