package com.workdo.gifts.ui.activity

import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActChangePasswordBinding
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch

class ActChangePassword : BaseActivity() {
    private lateinit var _binding: ActChangePasswordBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActChangePasswordBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnChangePassword.setOnClickListener {
            when {
                _binding.edPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_password_)
                    )
                }
                _binding.edConfirmPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_confirm_password)
                    )
                }
                _binding.edPassword.text.toString() != _binding.edConfirmPassword.text.toString() -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_valid_password)
                    )
                }
                else -> {
                    callChangePasswordApi()
                }
            }
        }
    }

    private fun callChangePasswordApi() {
        Utils.showLoadingProgress(this@ActChangePassword)
        val changehashmap = HashMap<String, String>()
        changehashmap["user_id"] =
            SharePreference.getStringPref(this, SharePreference.userId).toString()
        changehashmap["password"] = _binding.edPassword.text.toString()
        changehashmap["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActChangePassword)
                .setChangePassword(changehashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val changepasswordresponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            finish()
                        }
                        0 -> {
                            Utils.errorAlert(this@ActChangePassword,
                                changepasswordresponse?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(this@ActChangePassword,
                                response.body.message.toString())
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActChangePassword)
                    } else {
                        Utils.errorAlert(this@ActChangePassword, response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActChangePassword,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActChangePassword,
                        resources.getString(R.string.something_went_wrong))
                }

            }
        }
    }

}