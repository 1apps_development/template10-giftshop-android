package com.workdo.gifts.ui.activity

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.workdo.gifts.R
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActOrderPlaceBinding
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils

class ActOrderPlace : BaseActivity() {
    private lateinit var _binding: ActOrderPlaceBinding
    var title = ""
    var desc = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActOrderPlaceBinding.inflate(layoutInflater)
        init()
        if (!Utils.isLogin(this@ActOrderPlace)) {
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.GuestCartTotal,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.GuestCartList,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.GuestCartSubTotal,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.PaymentImage,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.DeliveryImage,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.GuestCouponData,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.TaxInfo,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.BillingDetails,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Payment_Type,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Payment_Comment,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_Id,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_Comment,
                ""
            )
            SharePreference.setStringPref(this@ActOrderPlace, SharePreference.cartCount, "")

        } else {
            SharePreference.setStringPref(this@ActOrderPlace, SharePreference.Coupon_Id, "")
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Coupon_Name,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Coupon_Code,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Coupon_Discount_Type,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Coupon_Discount_Number,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Coupon_Discount_Amount,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Coupon_Final_Amount,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Billing_Company_Name,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Billing_Address,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Billing_Postecode,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Billing_Country,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Billing_State,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Billing_City,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_Address,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_Postcode,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_Country,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_State,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_City,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Payment_Type,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Payment_Comment,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_Id,
                ""
            )
            SharePreference.setStringPref(
                this@ActOrderPlace,
                SharePreference.Delivery_Comment,
                ""
            )
            SharePreference.setStringPref(this@ActOrderPlace, SharePreference.cartCount, "")
        }
    }

    private fun init() {
        _binding.btnBackToHome.setOnClickListener {
            openActivity(ActMain::class.java)
        }
        title = intent.getStringExtra("orderTitle").toString()
        desc = intent.getStringExtra("desc").toString()
        val ordernumber = title.split("#")
        _binding.tvOrderNumber.text = "#".plus(ordernumber[1])
        _binding.tvyouorder.text = ordernumber[0]
        _binding.tvyouordersuccess.text = desc

        _binding.tvOrderNumber.setOnClickListener {
            dlgCopyOrderNumber(_binding.tvOrderNumber.text.toString())
        }
    }

    //TODO copy order number dialog
    private fun dlgCopyOrderNumber(message: String?) {
        val builder = AlertDialog.Builder(this@ActOrderPlace)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.order_number_copied)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("PhoneNumber", message)
            clipboard.setPrimaryClip(clip)
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        openActivity(ActMain::class.java)
    }

}