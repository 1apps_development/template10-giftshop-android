package com.workdo.gifts.ui.authentication

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActLoginOptionBinding
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.activity.ActMain
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject


class ActLoginOption : BaseActivity() {
    private lateinit var _binding: ActLoginOptionBinding
    //:::::::::::::::Google Login::::::::::::::::://
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val RC_SIGN_IN = 1
    //:::::::::::::::Facebook Login::::::::::::::::://
    private var callbackManager: CallbackManager? = null
    var callback: FacebookCallback<LoginResult>? = null
    var token = ""
    var getUserLoginType = "0"


    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActLoginOptionBinding.inflate(layoutInflater)
        init()

    }

    private fun init() {
        FirebaseApp.initializeApp(this@ActLoginOption)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful){
                    println("Failed to get token")
                    return@OnCompleteListener
                }
                token = task.result
                Log.d("Token-->", token)
            })
        Log.d("Token-->", token)
        Utils.getLog("Token== ", token)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        _binding.tvSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnLoginwithemail.setOnClickListener {
            openActivity(ActLogin::class.java)
        }

        _binding.btnGoogle.setOnClickListener {
            if (Utils.isCheckNetwork(this@ActLoginOption)) {
                mGoogleSignInClient!!.signOut()
                    .addOnCompleteListener(this, object : OnCompleteListener<Void> {
                        override fun onComplete(p0: Task<Void>) {
                            signInGoogle()
                        }
                    })
            } else {
                Utils.errorAlert(
                    this@ActLoginOption,
                    resources.getString(R.string.internet_connection_error)
                )
            }
        }

        _binding.btnFacebook.setOnClickListener {
            if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut()
            }
            LoginManager
                .getInstance()
                .logInWithReadPermissions(
                    this,
                    getFacebookPermissions()
                )
        }

        //::::::::::::::Facebook Login::::::::::::::::://
        FacebookSdk.setApplicationId(resources.getString(R.string.facebook_id));
        FacebookSdk.sdkInitialize(this@ActLoginOption)
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    updateFacebookUI(loginResult)
                }
                override fun onCancel() {}
                override fun onError(error: FacebookException) {
                    Toast.makeText(applicationContext, "" + error.message, Toast.LENGTH_LONG)
                        .show()
                }
            })


        }
    //Google
    private fun signInGoogle() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)!!
            nextGmailActivity(account)
        } catch (e: ApiException) {
            Log.e("Google Login", "signInResult:failed code=" + e.statusCode)
        }
    }

    @SuppressLint("HardwareIds")
    private fun nextGmailActivity(profile: GoogleSignInAccount?) {
        if (profile != null) {
            val loginType = "google"
            val firstName = profile.displayName
            val profileEmail = profile.email
            val profileId = profile.id
            val loginRequest = HashMap<String, String>()
            loginRequest["email"] = profileEmail.toString()
            loginRequest["token"] = token
            loginRequest["device_type"] = "android"
            loginRequest["google_id"] = profileId.toString()
            loginRequest["theme_id"]= getString(R.string.theme_id)
            callLoginApi(loginRequest)
            Utils.getLog("Google", "$firstName ++ $profileEmail++ $profileId")
        }
    }

    //Facebook
    private fun getFacebookPermissions(): List<String> {
        return listOf("email")
    }

    //::::::::::::::FacebookLogin:::::::::::::://
    private fun updateFacebookUI(loginResult: LoginResult) {
        val request = GraphRequest.newMeRequest(
            loginResult.accessToken
        ) { `object`, response -> `object`?.let { getFacebookData(it) } }
        val parameters = Bundle()
        Log.e("FaceSHA",request.accessToken.toString())
        parameters.putString(
            "fields",
            "id, first_name, last_name, email,age_range, gender, birthday, location"
        ) // Parámetros que pedimos a facebook
        request.parameters = parameters
        request.executeAsync()
    }

    private fun getFacebookData(`object`: JSONObject) {
        try {
            val profileId = `object`.getString("id")
            var name = ""
            if (`object`.has("first_name")) {
                name = `object`.getString("first_name")
            }
            if (`object`.has("last_name")) {
                name += " " + `object`.getString("last_name")
            }
            var profileEmail = ""
            if (`object`.has("email")) {
                profileEmail = `object`.getString("email")
            }
            val loginType = "facebook"
            val loginRequest = HashMap<String, String>()
            loginRequest["email"] = profileEmail.toString()
            loginRequest["token"] = token
            loginRequest["device_type"] = "android"
            loginRequest["facebook_id"] = profileId.toString()
            loginRequest["theme_id"] = getString(R.string.theme_id)
            Log.e("Request-->",loginRequest.toString())
            callLoginApi(loginRequest)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //TODO Login api calling
    private fun callLoginApi(loginRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActLoginOption)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoginOption).getLogin(loginRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loginResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            Log.e("UserId", loginResponse?.id.toString())
                            SharePreference.setBooleanPref(
                                this@ActLoginOption,
                                SharePreference.isLogin,
                                true
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userId,
                                loginResponse?.id.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userName,
                                loginResponse?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userFirstName,
                                loginResponse?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userLastName,
                                loginResponse?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userEmail,
                                loginResponse?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userProfile,
                                loginResponse?.image.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userMobile,
                                loginResponse?.mobile.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.token,
                                loginResponse?.token.toString()
                            )

                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.userProfile,
                                loginResponse?.image.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActLoginOption,
                                SharePreference.tokenType,
                                loginResponse?.tokenType.toString()
                            )
                            startActivity(
                                Intent(
                                    this@ActLoginOption,
                                    ActMain::class.java
                                )
                            )
                        }

                        0 -> {
                            Utils.errorAlert(this@ActLoginOption, loginResponse?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(this@ActLoginOption, loginResponse?.message.toString())
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActLoginOption)
                    }else{
                        Utils.errorAlert(
                            this@ActLoginOption,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoginOption,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoginOption,
                        "Something went wrong"
                    )
                }
            }
        }
    }

}