package com.workdo.gifts.ui.activity

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.gifts.R
import com.workdo.gifts.adapter.LoyalitylistAdapter
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActLoyaltyProgramBinding
import com.workdo.gifts.model.OrderListData
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.ExtensionFunctions.hide
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.PaginationScrollListener
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch

class ActLoyaltyProgram : BaseActivity() {
    private lateinit var _binding: ActLoyaltyProgramBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<OrderListData>()
    private lateinit var loyalitylistAdapter: LoyalitylistAdapter
    private var manager: LinearLayoutManager? = null

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActLoyaltyProgramBinding.inflate(layoutInflater)
        init()
    }

    private fun init(){
        manager = LinearLayoutManager(this@ActLoyaltyProgram)
        orderListAdapter(orderList)
        pagination()
        _binding.ivBack.setOnClickListener { finish() }
        _binding.btnCopyURl.setOnClickListener {
            dlgCopyOrderNumber(_binding.tvURL.text.toString())

        }

        loyalityProgram()
    }

    private fun dlgCopyOrderNumber(message: String?) {
        val builder = AlertDialog.Builder(this@ActLoyaltyProgram)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.url_copied)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Url_copied", message)
            clipboard.setPrimaryClip(clip)
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callOrderList()
            }
        }
        _binding.rvLoyality.addOnScrollListener(paginationListener)
    }

    //TODO delivery list api
    private fun loyalityProgram() {
        Utils.showLoadingProgress(this@ActLoyaltyProgram)
        lifecycleScope.launch {
            val request=HashMap<String,String>()
            request["theme_id"]=resources.getString(R.string.theme_id)
            when (val response = ApiClient.getClient(this@ActLoyaltyProgram)
                .loyalityProgramJson(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loyalityProgramResponse = response.body.data?.loyalityProgram
                    when (response.body.status) {
                        1 -> {
                            _binding.tvProgramTitle.text =
                                loyalityProgramResponse?.loyalityProgramTitle
                            _binding.tvProgramDesc.text =
                                loyalityProgramResponse?.loyalityProgramDescription

                            _binding.tvYourFriend.text =
                                loyalityProgramResponse?.loyalityProgramCopyThisLinkAndSendToYourFriends
                            loyalityReward(loyalityProgramResponse?.loyalityProgramYourCash)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyaltyProgram,
                                loyalityProgramResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyaltyProgram,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActLoyaltyProgram)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyaltyProgram,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyaltyProgram,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyaltyProgram,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun loyalityReward(loyalityProgramYourCash: String?) {
        val loyalityReward = HashMap<String, String>()
        loyalityReward["user_id"] =
            SharePreference.getStringPref(this@ActLoyaltyProgram, SharePreference.userId)
                .toString()
        loyalityReward["theme_id"]=resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyaltyProgram)
                .loyalityReward(loyalityReward)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loyalityRewardResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            _binding.tvProgramYourCash.text =
                                loyalityProgramYourCash?.plus(": ").plus("+")
                                    .plus(loyalityRewardResponse?.point).plus(
                                        SharePreference.getStringPref(
                                            this@ActLoyaltyProgram,
                                            SharePreference.currency_name
                                        )
                                    ).toString()
                            _binding.tvURL.text = ApiClient.ImageURL.BASE_URL.toString()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyaltyProgram,
                                loyalityRewardResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyaltyProgram,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActLoyaltyProgram)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyaltyProgram,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyaltyProgram,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyaltyProgram,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun callOrderList() {
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] =
            SharePreference.getStringPref(this@ActLoyaltyProgram, SharePreference.userId).toString()
        categoriesProduct["theme_id"]=resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyaltyProgram)
                .getOrderList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val OrderListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvLoyality.show()
                                currentPage =
                                    OrderListResponse?.currentPage!!.toInt()
                                total_pages =
                                    OrderListResponse.lastPage!!.toInt()
                                OrderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvLoyality.hide()
                            }
                            loyalitylistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyaltyProgram,
                                OrderListResponse?.data?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyaltyProgram,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActLoyaltyProgram)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyaltyProgram,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyaltyProgram,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyaltyProgram,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun orderListAdapter(orderHistorylist: ArrayList<OrderListData>) {
        _binding.rvLoyality.layoutManager = manager
        loyalitylistAdapter =
            LoyalitylistAdapter(this@ActLoyaltyProgram, orderHistorylist) { i: Int, s: String ->

            }
        _binding.rvLoyality.adapter = loyalitylistAdapter
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        orderList.clear()
        callOrderList()
    }


}