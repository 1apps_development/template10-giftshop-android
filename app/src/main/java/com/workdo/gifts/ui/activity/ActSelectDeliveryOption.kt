package com.workdo.gifts.ui.activity

import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.gifts.R
import com.workdo.gifts.adapter.DeliveryAdapter
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActSelectDeliveryOptionBinding
import com.workdo.gifts.model.DeliveryData
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.ExtensionFunctions.hide
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch


class ActSelectDeliveryOption : BaseActivity() {
    private lateinit var _binding: ActSelectDeliveryOptionBinding
    private var deliveryList = ArrayList<DeliveryData>()
    private lateinit var deliveryAdapter: DeliveryAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSelectDeliveryOptionBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.ivBack.setOnClickListener { finish() }

        _binding.btnContinue.setOnClickListener {

            comment = _binding.edNote.text.toString()
            SharePreference.setStringPref(
                this@ActSelectDeliveryOption,
                SharePreference.Delivery_Comment,
                comment.toString()
            )
            openActivity(ActSelectPaymentOption::class.java)

        }
        manager =
            GridLayoutManager(this@ActSelectDeliveryOption, 1, GridLayoutManager.VERTICAL, false)

    }

    //TODO delivery list api
    private fun callDeliveryList() {
        Utils.showLoadingProgress(this@ActSelectDeliveryOption)
        val hashMap = HashMap<String, String>()
        hashMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSelectDeliveryOption)
                .deliveryList(hashMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvDelivery.show()
                                _binding.vieww.hide()
                                paymentListResponse.data?.let {
                                    deliveryList.addAll(it)
                                }
                            } else {
                                _binding.rvDelivery.hide()
                                _binding.vieww.show()
                            }
                            deliveryAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActSelectDeliveryOption,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSelectDeliveryOption,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActSelectDeliveryOption)
                    } else {
                        Utils.errorAlert(this@ActSelectDeliveryOption,
                            response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSelectDeliveryOption,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSelectDeliveryOption,
                        resources.getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

    //TODO delivery list adapter
    private fun DeliveryListAdapter(deliveryList: ArrayList<DeliveryData>) {
        _binding.rvDelivery.layoutManager = manager
        deliveryAdapter =
            DeliveryAdapter(this@ActSelectDeliveryOption, deliveryList) { i: Int, s: String ->
                if (deliveryList[i].isSelect == true) {
                    SharePreference.setStringPref(
                        this@ActSelectDeliveryOption,
                        SharePreference.Delivery_Id,
                        deliveryList[i].id.toString()
                    )

                    SharePreference.setStringPref(
                        this@ActSelectDeliveryOption,
                        SharePreference.DeliveryImage,
                        deliveryList[i].imagePath.toString()
                    )
                }
            }
        _binding.rvDelivery.adapter = deliveryAdapter
    }

    override fun onResume() {
        super.onResume()
        deliveryList.clear()
        DeliveryListAdapter(deliveryList)
        callDeliveryList()
    }

}