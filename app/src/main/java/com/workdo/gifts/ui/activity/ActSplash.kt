package com.workdo.gifts.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Handler
import android.os.Looper
import android.util.Base64
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActSplashBinding
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class ActSplash : BaseActivity() {
    private lateinit var _binding:ActSplashBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSplashBinding.inflate(layoutInflater)
        init()
    }

    private fun init(){

        lifecycleScope.launch {
            baseUrl()
            getExtraUrl()
            printKeyHash(this@ActSplash)
            Utils.getLog("getShaKey", printKeyHash(this@ActSplash)!!)
            Handler(Looper.getMainLooper()).postDelayed({
                openActivity(ActMain::class.java)
                finish()
            }, 2000)
        }

    }

    private suspend fun baseUrl() {
        val hashmap = HashMap<String, String>()
        hashmap["theme_id"] = getString(R.string.theme_id)
        withContext(lifecycleScope.coroutineContext) {
            when (val response = ApiClient.defaultClient.baseUrl(hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val responseData = response.body
                    when (responseData.status) {
                        1 -> {

                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.BaseUrl,
                                responseData.data?.baseUrl.toString().plus("/")
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.ImageUrl,
                                responseData.data?.imageUrl.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.PaymentUrl,
                                responseData.data?.paymentUrl.toString().plus("/")
                            )
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                responseData.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                responseData.message.toString()
                            )
                            startActivity(Intent(this@ActSplash, ActWelCome::class.java))

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActSplash)
                    } else {
                        Utils.errorAlert(
                            this@ActSplash,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO exter url api
    private suspend fun getExtraUrl() {
        val exterURL = HashMap<String, String>()
        exterURL["theme_id"] =getString(R.string.theme_id)
        withContext(lifecycleScope.coroutineContext){
            when (val response = ApiClient.getClient(this@ActSplash).extraUrl(exterURL)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val extraUrl = response.body
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.Terms,
                                extraUrl.data?.terms.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.Contact_Us,
                                extraUrl.data?.contactUs.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.insta,
                                extraUrl.data?.insta.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.youtube,
                                extraUrl.data?.youtube.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.messanger,
                                extraUrl.data?.messanger.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.twitter,
                                extraUrl.data?.twitter.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.returnPolicy,
                                extraUrl.data?.returnPolicy.toString()
                            )
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                extraUrl.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                extraUrl.message.toString()
                            )
                            openActivity(ActWelCome::class.java)

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActSplash)
                    } else {
                        Utils.errorAlert(
                            this@ActSplash,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            val packageName = context.applicationContext.packageName
            packageInfo = context.packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", context.applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }

}