package com.workdo.gifts.ui.authentication

import android.view.View
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActSuccessfullBinding
import com.workdo.gifts.ui.activity.ActMain

class ActSuccessfull : BaseActivity() {
    private lateinit var _binding: ActSuccessfullBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSuccessfullBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnGotoyourstore.setOnClickListener {
            openActivity(ActMain::class.java)
        }
    }
}
