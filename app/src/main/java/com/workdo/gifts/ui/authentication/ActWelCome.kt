package com.workdo.gifts.ui.authentication

import android.content.Intent
import android.view.View
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActWelcomeBinding
import com.workdo.gifts.ui.activity.ActMain

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelcomeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelcomeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnGuest.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    ActMain::class.java
                )
            )
            finish()
        }

    }

}