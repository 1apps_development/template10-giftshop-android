package com.workdo.gifts.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.gifts.databinding.ActWishListBinding

class ActWishList : AppCompatActivity() {
    private lateinit var binding: ActWishListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActWishListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupAdapter()
        initClickListeners()
    }

    private fun setupAdapter()
    {

    }

    private fun initClickListeners()
    {


    }
}