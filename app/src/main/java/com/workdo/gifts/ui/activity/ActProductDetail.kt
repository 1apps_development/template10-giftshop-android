package com.workdo.gifts.ui.activity

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.SnapHelper
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.gifts.R
import com.workdo.gifts.adapter.DescriptionAdapter
import com.workdo.gifts.adapter.RattingAdapter
import com.workdo.gifts.adapter.ReletedProductAdapter
import com.workdo.gifts.adapter.ViewPagerAdapter
import com.workdo.gifts.adapter.variant.VariantAdapter
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActProductDetailBinding
import com.workdo.gifts.databinding.DlgConfirmBinding
import com.workdo.gifts.model.*
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.Constants
import com.workdo.gifts.utils.ExtensionFunctions.capitalized
import com.workdo.gifts.utils.ExtensionFunctions.hide
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.PaginationScrollListener
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ActProductDetail : BaseActivity() {
    private lateinit var _binding: ActProductDetailBinding
    private lateinit var adapter: ViewPagerAdapter
    private lateinit var variantAdapter: VariantAdapter
    private var reletedProductdataList = ArrayList<RelatedProductDataItem>()
    private lateinit var reletedProductadapter: ReletedProductAdapter
    private var manager: GridLayoutManager? = null

    var releteddataitem = RelatedProductDataItem()

    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0

    private var rattingValue = "0"
    private var product_id = ""
    private var variant_id = ""
    private var variantName = ""
    private var finalPrice = ""
    private var disCountPrice = ""
    private var originalPrice = ""
    private var productInfo = ProductInfo()
    private var productStock = ""
    private var currency = ""
    private var isWishList = false
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActProductDetailBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        manager = GridLayoutManager(this@ActProductDetail, 1, GridLayoutManager.HORIZONTAL, false)
        relatedProductPagination()

        currency = SharePreference.getStringPref(this@ActProductDetail, SharePreference.currency)
            .toString()

        if (Utils.isLogin(this@ActProductDetail)) {
            _binding.btnFeedback.show()
            _binding.ivWishlist.show()
        } else {
            _binding.btnFeedback.hide()
            _binding.ivWishlist.hide()

        }

        initClickListeners()
        callProductDetailApi()

        _binding.tvCount.text =
            SharePreference.getStringPref(this@ActProductDetail, SharePreference.cartCount)
                .toString()
        _binding.btnFeedback.setOnClickListener {
            openFeedbackDialog()
        }

        _binding.btnMore.setOnClickListener {
            val contactUs =
                SharePreference.getStringPref(this@ActProductDetail, SharePreference.returnPolicy)
                    .toString()
            val uri: Uri = Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }

    }

    private fun relatedProductPagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callRelectedProductApi()
            }

        }
        _binding.rvRecentProducts.addOnScrollListener(paginationListener)
    }

    override fun onResume() {
        super.onResume()

        reletedProductdataList.clear()
        Relectedproductadapter(reletedProductdataList)
        callRelectedProductApi()

        _binding.tvCount.text =
            SharePreference.getStringPref(this@ActProductDetail, SharePreference.cartCount)
                .toString()
    }

    private fun Relectedproductadapter(reletedProductdataList: ArrayList<RelatedProductDataItem>) {
        _binding.rvRecentProducts.layoutManager = manager
        reletedProductadapter =
            ReletedProductAdapter(
                this@ActProductDetail,
                reletedProductdataList
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActProductDetail,
                            SharePreference.isLogin
                        )
                    ) {
                        if (reletedProductdataList[i].inWhishlist == false) {
                            callWishlist(
                                "add", reletedProductdataList[i].id, "reletedproductList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                reletedProductdataList[i].id,
                                "reletedproductList",
                                i
                            )
                        }
                    } else {
                        startActivity(Intent(this@ActProductDetail, ActWelCome::class.java))
                    }
                } else if (s == Constants.CartClick) {
                    val data = reletedProductdataList[i]
                    if (!Utils.isLogin(this@ActProductDetail)) {
                        guestUserAddToCart(data, reletedProductdataList[i].id, true,reletedProductdataList[i].defaultVariantId.toString())
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(
                                this@ActProductDetail,
                                SharePreference.userId
                            )
                                .toString()
                        addtocart["product_id"] = reletedProductdataList[i].id.toString()
                        addtocart["variant_id"] =
                            reletedProductdataList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            reletedProductdataList[i].id,
                            reletedProductdataList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActProductDetail, ActProductDetail::class.java)
                    intent.putExtra(Constants.ProductId, reletedProductdataList[i].id.toString())
                    startActivityForResult.launch(intent)
                }
            }
        _binding.rvRecentProducts.adapter = reletedProductadapter
    }

    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(this@ActProductDetail)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActProductDetail, SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "reletedproductList" -> {
                                        reletedProductdataList[position].inWhishlist = true
                                        reletedProductadapter.notifyItemChanged(position)
                                    }
                                    else -> {
                                        isWishList = true
                                        _binding.ivWishlist.background =
                                            ResourcesCompat.getDrawable(
                                                resources,
                                                R.drawable.ic_wishlist,
                                                null
                                            )

                                    }


                                }
                            } else {
                                when (itemType) {
                                    "reletedproductList" -> {
                                        reletedProductdataList[position].inWhishlist = false
                                        reletedProductadapter.notifyItemChanged(position)
                                    }
                                    else -> {
                                        isWishList = false
                                        _binding.ivWishlist.background =
                                            ResourcesCompat.getDrawable(
                                                resources,
                                                R.drawable.ic_diswishlist,
                                                null
                                            )

                                    }


                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                wishlistResponse?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetail, ActWelCome::class.java))
                            this@ActProductDetail.finish()
                            this@ActProductDetail.finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun callRelectedProductApi() {
        Utils.showLoadingProgress(this@ActProductDetail)
        val reletedproduct = HashMap<String, String>()
        reletedproduct["product_id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        reletedproduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(this@ActProductDetail, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActProductDetail)
                        .relatedProductGuest(reletedproduct)
                } else {
                    ApiClient.getClient(this@ActProductDetail)
                        .relatedProduct(reletedproduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val recentproduct = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {

                                _binding.rvRecentProducts.show()

                                currentPage =
                                    recentproduct?.currentPage!!.toInt()
                                total_pages =
                                    recentproduct.lastPage!!.toInt()
                                recentproduct.data?.let {
                                    reletedProductdataList.addAll(it)
                                }

                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvRecentProducts.hide()
                            }
                            reletedProductadapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                recentproduct?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                recentproduct?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToken(this@ActProductDetail)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }


    private fun openFeedbackDialog() {
        var dialog: Dialog? = null
        try {
            dialog?.dismiss()
            dialog = Dialog(this@ActProductDetail, R.style.AppCompatAlertDialogStyleBig)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val mInflater = LayoutInflater.from(this@ActProductDetail)
            val mView = mInflater.inflate(R.layout.dlg_feedback, null, false)
            val btnCancel: AppCompatButton = mView.findViewById(R.id.btnCancel)
            val btnRateNow: AppCompatButton = mView.findViewById(R.id.btnRateNow)
            val ivStar1: ImageView = mView.findViewById(R.id.ivStar1)
            val ivStar2: ImageView = mView.findViewById(R.id.ivStar2)
            val ivStar3: ImageView = mView.findViewById(R.id.ivStar3)
            val ivStar4: ImageView = mView.findViewById(R.id.ivStar4)
            val ivStar5: ImageView = mView.findViewById(R.id.ivStar5)
            val title: EditText = mView.findViewById(R.id.edTitle)
            val edNote: AppCompatEditText = mView.findViewById(R.id.edNote)
            ivStar1.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
            ivStar2.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
            ivStar3.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
            ivStar4.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
            ivStar5.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
            ivStar1.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                rattingValue = "1"
            }
            ivStar2.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                rattingValue = "2"
            }
            ivStar3.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                rattingValue = "3"
            }
            ivStar4.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.white)
                rattingValue = "4"
            }
            ivStar5.setOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.pink)
                rattingValue = "5"
            }
            val finalDialog: Dialog = dialog
            btnRateNow.setOnClickListener {
                when {
                    rattingValue == "0" -> {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    edNote.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    title.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    else -> {
                        val ratting = HashMap<String, String>()
                        ratting["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
                        ratting["user_id"] =
                            SharePreference.getStringPref(
                                this@ActProductDetail,
                                SharePreference.userId
                            )
                                .toString()
                        ratting["rating_no"] = rattingValue.toString()
                        ratting["title"] = title.text.toString()
                        ratting["description"] = edNote.text.toString()
                        ratting["theme_id"] = resources.getString(R.string.theme_id)

                        callProductRatting(ratting)
                        finalDialog.dismiss()
                    }
                }
            }
            btnCancel.setOnClickListener {
                finalDialog.dismiss()
            }
            dialog.setContentView(mView)
            dialog.show()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun callProductRatting(ratting: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .productRating(ratting)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val rattingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            callProductDetailApi()
                            Utils.successAlert(
                                this@ActProductDetail,
                                rattingResponse?.message.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                rattingResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun setupVariantAdapter(variantList: ArrayList<VariantItem>) {
        variantAdapter = VariantAdapter(variantList, this@ActProductDetail, variantName) {
            val selectedVariantList = ArrayList<String>()
            for (i in 0 until variantList.size) {
                for (j in 0 until variantList[i].dataItem?.size!!) {
                    if (variantList[i].dataItem!![j].isSelect == true) {
                        selectedVariantList.add(variantList[i].dataItem!![j].name?.trim().toString())
                    }
                }
            }
            val filterValue = selectedVariantList.joinToString(separator = "-") { it.replace(" ","") }
            callVariantApi(filterValue, true)
        }

        _binding.rvVariant.apply {
            layoutManager = LinearLayoutManager(this@ActProductDetail)
            adapter = variantAdapter
            isNestedScrollingEnabled = true
        }
    }
    private fun callFirstPositionVariantCheck(variantList: ArrayList<VariantItem>) {
        if (variantList.size != 0) {
            val selectedVariantList = ArrayList<String>()

            for (i in 0 until variantList.size) {
                if (variantList[i].value?.size != 0) {

                    if (variantName.isNotEmpty()) {
                        selectedVariantList.add(variantName.trim())
                        break

                    } else {
                        selectedVariantList.add(variantList[i].value?.get(0)?.trim() ?: "")
                        break

                    }

                }
            }

            val filterValue = selectedVariantList.joinToString(separator = "-") { it.trim() }
            callVariantApi(filterValue, false)
        }
    }

    private fun initClickListeners() {

        _binding.ivBack.setOnClickListener {
            finish()
        }

        _binding.ivWishlist.setOnClickListener {
            if (isWishList) {
                callWishlist("remove", product_id.toInt(), "", 0)
            } else {
                callWishlist("add", product_id.toInt(), "", 0)

            }
        }

        _binding.clcart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
        _binding.btnAddtoCard.setOnClickListener {
            if (productStock == "0") {
                if (Utils.isLogin(this@ActProductDetail)) {
                    val notifyUser = HashMap<String, String>()
                    notifyUser["user_id"] =
                        SharePreference.getStringPref(
                            this@ActProductDetail,
                            SharePreference.userId
                        )
                            .toString()
                    notifyUser["product_id"] = product_id.toString()
                    notifyUser["theme_id"] = resources.getString(R.string.theme_id)

                    notifyUser(notifyUser)
                } else {
                    val intent = Intent(this@ActProductDetail, ActWelCome::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
            } else {
                val data = releteddataitem
                if (Utils.isLogin(this@ActProductDetail)) {
                    val addtocart = HashMap<String, String>()
                    addtocart["user_id"] =
                        SharePreference.getStringPref(
                            this@ActProductDetail,
                            SharePreference.userId
                        )
                            .toString()
                    addtocart["product_id"] = product_id
                    addtocart["variant_id"] = variant_id
                    addtocart["qty"] = "1"
                    addtocart["theme_id"] = resources.getString(R.string.theme_id)

                    addtocartApi(
                        addtocart,
                        product_id.toInt(),
                        variant_id.toInt(), 0
                    )
                } else {
                    guestUserAddToCart(data, product_id.toInt(), false,variant_id)
                }
            }
        }
    }

    private fun notifyUser(notifyUser: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .notifyUser(notifyUser)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                addtocart?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActProductDetail.finish()
                            this@ActProductDetail.finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }

    }

    private fun dlgAlreadyCart(product_id: String, i: Int, variant_id: String) {
        val builder = AlertDialog.Builder(this@ActProductDetail)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActProductDetail)) {
                cartqty["product_id"] = product_id
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        this@ActProductDetail,
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = variant_id
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = resources.getString(R.string.theme_id)
                cartQtyApi(cartqty)
            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActProductDetail,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                val cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                val count = cartList[i].qty ?: 0
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)

            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,

        ) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActProductDetail,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActProductDetail,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActProductDetail,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActProductDetail)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        confirmDialogBinding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    private fun guestUserAddToCart(
        data: RelatedProductDataItem,
        id: Int?,
        fromRelatedItem: Boolean,
        variantId:String
    ) {
        val cartData = cartData(data, fromRelatedItem)

        val cartListData =
            SharePreference.getStringPref(this@ActProductDetail, SharePreference.GuestCartList)
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        val cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)

        if (cartList == null) {
            dlgConfirm(cartData.name + " " + R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, id.toString(),variantId)) {
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.id.toString(), i, data.defaultVariantId.toString())
                }
            }
        } else {
            dlgConfirm(cartData.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String,variantId: String): Boolean {
        return cartList.filter { it.productId == id && it.variantId==variantId }.size == 1
    }


    private val startActivityForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data
            when (resultCode) {
                Activity.RESULT_OK -> {
                    reletedProductdataList.clear()
                    callRelectedProductApi()

                }
            }
        }


    private fun cartData(data: RelatedProductDataItem, fromRelatedItem: Boolean): ProductListItem {

        return if (fromRelatedItem) {
            ProductListItem(
                data.coverImagePath.toString(),
                data.defaultVariantId.toString(),
                data.finalPrice.toString(),
                data.discountPrice.toString(),
                data.id.toString(),
                1,
                data.name.toString(),
                "",
                "",
                "",
                data.originalPrice.toString()
            )
        } else {
            ProductListItem(
                productInfo.coverImagePath.toString(),
                variant_id,
                finalPrice,
                disCountPrice,
                productInfo.id.toString(),
                1,
                productInfo.name.toString(),
                "",
                "",
                variantName,
                originalPrice
            )
        }
    }


    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?,
    ) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartData = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                this@ActProductDetail,
                                SharePreference.cartCount,
                                cartData?.count.toString()
                            )
                            setCount(cartData?.count)
                        }
                        0 -> {
                            if (response.body.data?.isOutOFStock == 1) {
                                Utils.errorAlert(
                                    this@ActProductDetail,
                                    response.body.data?.message.toString()
                                )
                            } else {
                                dlgAlreadyCart(id.toString(), i!!, defaultVariantId.toString())

                            }

                        }

                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                cartData?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActProductDetail.finish()
                            this@ActProductDetail.finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun setupDetailData(data: ProductDetailData) {
        _binding.tvProductName.text = data.productInfo?.name

        isWishList = data.productInfo?.inWhishlist ?: false

        if (data.productInfo?.inWhishlist == true) {
            _binding.ivWishlist.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_wishlist, null)
        } else {
            _binding.ivWishlist.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_diswishlist, null)
        }

         _binding.tvProductDesc.text = data.productInfo?.description.toString()
        _binding.tvProductTag.text = data.productInfo?.tagApi?.capitalized()
        _binding.tvProductPrice.text = Utils.getPrice(data.productInfo?.finalPrice.toString())
        _binding.tvDiscountPrice.text =
            currency.plus(Utils.getPrice(data.productInfo?.originalPrice.toString()))
        _binding.ivratting.rating = data.productInfo?.averageRating?.toFloat() ?: 0.0f
        _binding.tvRatting.text = data.productInfo?.averageRating?.toFloat().toString()

        val variantList = ArrayList<VariantItem>()
        data.variant?.let { variantList.addAll(it) }

        setupVariantAdapter(variantList)
        callFirstPositionVariantCheck(variantList)

        val imageList = ArrayList<ProductImageItem>()
        data.productImage?.let { imageList.addAll(it) }
        if (imageList.size == 0) {
            _binding.viewPager.hide()
            _binding.vieww.show()
        } else {
            _binding.viewPager.show()
            _binding.vieww.hide()
            adapterSetup(imageList)
            setCurrentIndicator(0)
        }
        _binding.ivratting.rating = data.productInfo?.averageRating?.toFloat() ?: 0.0f
        if (data.productReview?.size == 0) {
            _binding.rvReview.hide()
            _binding.tvTestimonials.hide()
            _binding.ivComma.hide()
        } else {
            _binding.tvTestimonials.show()
            _binding.rvReview.show()
            _binding.ivComma.show()
            _binding.rvReview.layoutManager =
                GridLayoutManager(this@ActProductDetail, 1, GridLayoutManager.HORIZONTAL, false)
            val adapter =
                data.productReview?.let {
                    RattingAdapter(this@ActProductDetail, it) { id: String, name: String ->
                    }
                }
            _binding.rvReview.adapter = adapter

        }

        val otherDescriptionArray = data.productInfo?.otherDescriptionArray
        otherDescriptionArray?.removeAll { it.description?.isEmpty() == true }
        if (data.productInfo?.otherDescriptionArray?.size == 0) {
            _binding.rvDescription.hide()
        } else {
            _binding.rvDescription.show()

            _binding.rvDescription.apply {
                layoutManager = LinearLayoutManager(this@ActProductDetail)
                adapter = otherDescriptionArray?.let { DescriptionAdapter(it) }
            }
        }
    }

    private fun callVariantApi(variant_sku: String,fromClick:Boolean) {

        if(fromClick)
        {
            Utils.showLoadingProgress(this@ActProductDetail)

        }
        val productDetailRequest = HashMap<String, String>()
        productDetailRequest["product_id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        productDetailRequest["variant_sku"] = variant_sku
        productDetailRequest["theme_id"] = resources.getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActProductDetail).variantStock(productDetailRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 1) {
                        val data = response.body.data
                        _binding.tvProductPrice.text = Utils.getPrice(data?.finalPrice.toString())
                        _binding.tvCurrency.text =
                            SharePreference.getStringPref(
                                this@ActProductDetail,
                                SharePreference.currency
                            ).toString()
                        _binding.tvDiscountPrice.text =
                            currency.plus(Utils.getPrice(data?.originalPrice.toString()))
                        variant_id = data?.id.toString()
                        disCountPrice = data?.discountPrice.toString()
                        originalPrice = data?.originalPrice.toString()
                        finalPrice = data?.finalPrice.toString()
                        variantName = data?.variant.toString()
                        productStock = data?.stock.toString()

                        if (data?.stock == 0) {

                            _binding.btnAddtoCard.text = getString(R.string.notify_me)
                            _binding.tvOutStock.show()

                        } else {

                            _binding.tvOutStock.hide()
                            _binding.btnAddtoCard.text = getString(R.string.add_to_cart)

                        }
                    }

                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    private fun callProductDetailApi() {
        Utils.showLoadingProgress(this@ActProductDetail)
        val productDetailRequest = HashMap<String, String>()
        productDetailRequest["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        productDetailRequest["theme_id"] = resources.getString(R.string.theme_id)

        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActProductDetail, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActProductDetail)
                        .productDetailGuest(productDetailRequest)
                } else {
                    ApiClient.getClient(this@ActProductDetail).productDetail(productDetailRequest)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 1) {
                        variantName = response.body.data?.productInfo?.defaultVariantName.toString()

                        response.body.data?.let { setupDetailData(it) }
                        productInfo = response.body.data?.productInfo!!
                        product_id = response.body.data.productInfo.id.toString()
                        variant_id = response.body.data.productInfo.defaultVariantId.toString()
                        disCountPrice = response.body.data.productInfo.discountPrice.toString()
                        originalPrice = response.body.data.productInfo.originalPrice.toString()
                        finalPrice = response.body.data.productInfo.finalPrice.toString()

                        _binding.tvReturn.text =
                            response.body.data.productInfo.retuen_text.toString()
                        if (response.body.data.productInfo.is_review == 0) {
                            if (Utils.isLogin(this@ActProductDetail)) {
                                _binding.btnFeedback.show()
                            } else {
                                _binding.btnFeedback.hide()
                            }
                        } else {
                            _binding.btnFeedback.hide()
                        }
                        productStock = response.body.data.productInfo.productStock.toString()
                        Log.e("productStock", productStock)

                        if (response.body.data.variant?.size == 0) {

                            if ((response.body.data.productInfo.productStock ?: 0) <= 0) {

                                _binding.btnAddtoCard.text = getString(R.string.notify_me)
                                _binding.tvOutStock.show()
                            } else {
                                _binding.tvOutStock.hide()
                                _binding.btnAddtoCard.text = getString(R.string.add_to_cart)
                            }
                        }
                    }
                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    private fun adapterSetup(imageList: ArrayList<ProductImageItem>) {
        adapter = ViewPagerAdapter(imageList, this@ActProductDetail)
        _binding.viewPager.adapter = adapter
        _binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {}
            override fun onPageSelected(i: Int) {
                setCurrentIndicator(i)
            }

            override fun onPageScrollStateChanged(i: Int) {}
        })
        setupIndicator()
    }

    private fun setupIndicator() {
        _binding.indicatorContainer.removeAllViews()
        val indicators = arrayOfNulls<ImageView>(adapter.count)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i]?.apply {
                this.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetail,
                        R.drawable.tab_indicator_default
                    )
                )
                this.layoutParams = layoutParams
            }
            _binding.indicatorContainer.addView(indicators[i])
        }
    }

    private fun setCurrentIndicator(index: Int) {
        val childCount = _binding.indicatorContainer.childCount
        for (i in 0 until childCount) {
            val imageview = _binding.indicatorContainer[i] as ImageView
            if (i == index) {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetail,
                        R.drawable.tab_indicator_selected
                    )
                )
            } else {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetail,
                        R.drawable.tab_indicator_default
                    )
                )

            }
        }
    }


}