package com.workdo.gifts.ui.activity

import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.gifts.R
import com.workdo.gifts.adapter.CartListAdapter
import com.workdo.gifts.adapter.CartTaxListAdapter
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActShoppingCartBinding
import com.workdo.gifts.databinding.DlgUserSelectionBinding
import com.workdo.gifts.model.ProductListItem
import com.workdo.gifts.model.TaxItem
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.Constants
import com.workdo.gifts.utils.ExtensionFunctions.hide
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type
import java.util.*

class ActShoppingCart : BaseActivity() {
    private lateinit var _binding: ActShoppingCartBinding
    private var cartList = ArrayList<ProductListItem>()
    private lateinit var cartListAdapter: CartListAdapter
    private var manager: GridLayoutManager? = null
    private var taxlist = ArrayList<TaxItem>()
    private lateinit var taxlistAdapter: CartTaxListAdapter
    private var managerTax: GridLayoutManager? = null

    private  var count = 1
    private var subTotal = ""
    private var cartItemTotal = ""
    private var finalTotal = ""
    private var currency = ""
    private  var firstTime = "0"

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActShoppingCartBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        managerTax = GridLayoutManager(this@ActShoppingCart, 1, GridLayoutManager.HORIZONTAL, false)
        _binding.tvCouponCodeName.hide()
        _binding.tvCouponCodePrice.hide()

        currency = SharePreference.getStringPref(this@ActShoppingCart, SharePreference.currency).toString()

        _binding.ivBack.setOnClickListener { finish() }
        manager = GridLayoutManager(this@ActShoppingCart, 1, GridLayoutManager.VERTICAL, false)

        _binding.btnApply.setOnClickListener {
            if (_binding.edCouponCode.text?.isEmpty() == true) {
                Utils.errorAlert(
                    this@ActShoppingCart,
                    resources.getString(R.string.please_enter_promocode)
                )
            } else {
                val applyCoupon = HashMap<String, String>()
                applyCoupon["coupon_code"] = _binding.edCouponCode.text.toString()
                applyCoupon["sub_total"] = subTotal
                applyCoupon["theme_id"] = getString(R.string.theme_id)
                applyCoupon(applyCoupon)
            }
        }

        _binding.btnProceedtoCheckout.setOnClickListener {

            if (Utils.isLogin(this@ActShoppingCart)) {
                val cartcheck = HashMap<String, String>()
                cartcheck["user_id"] =
                    SharePreference.getStringPref(this@ActShoppingCart, SharePreference.userId)
                        .toString()
                cartcheck["theme_id"] = getString(R.string.theme_id)
                cartCheckApi(cartcheck)
            } else {
                dlg_CheckGuestUser()
            }

        }

        SharePreference.setStringPref(this@ActShoppingCart, SharePreference.Coupon_Id, "")
        SharePreference.setStringPref(this@ActShoppingCart, SharePreference.Coupon_Name, "")
        SharePreference.setStringPref(this@ActShoppingCart, SharePreference.Coupon_Code, "")
        SharePreference.setStringPref(
            this@ActShoppingCart,
            SharePreference.Coupon_Discount_Type,
            ""
        )
        SharePreference.setStringPref(
            this@ActShoppingCart,
            SharePreference.Coupon_Discount_Amount,
            ""
        )
        SharePreference.setStringPref(this@ActShoppingCart, SharePreference.Coupon_Final_Amount, "")
        SharePreference.setStringPref(this@ActShoppingCart, SharePreference.GuestCouponData, "")

    }

    //TODO Checkout or continue dialog
    private fun dlg_CheckGuestUser() {
        val confirmDialogBinding = DlgUserSelectionBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActShoppingCart)
        dialog.setContentView(confirmDialogBinding.root)

        confirmDialogBinding.tvContinueAsGuest.setOnClickListener {
            dialog.dismiss()
            openActivity(ActBillingDetails::class.java)
        }

        confirmDialogBinding.tvContinueAsUser.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this@ActShoppingCart, ActWelCome::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
        confirmDialogBinding.tvappname.text = getText(R.string.app_name)
        dialog.show()
    }

    //TODO cart check api
    private fun cartCheckApi(cartcheck: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActShoppingCart)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActShoppingCart)
                .cartcheck(cartcheck)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActShoppingCart,
                                    ActBillingDetails::class.java
                                )
                            )
                        }
                        0 -> {
                            _binding.tvCouponCodeName.hide()
                            _binding.tvCouponCodePrice.hide()
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                cartListResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                cartListResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActShoppingCart)
                    } else {
                        Utils.errorAlert(
                            this@ActShoppingCart,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActShoppingCart,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActShoppingCart,
                    resources.getString(R.string.something_went_wrong))
                }
            }
        }
    }

    //TODO guest user tax data set
    private fun taxGuest(cartList: ArrayList<ProductListItem>) {
        val subTotalValue = cartList.sumOf { it.finalPrice?.toDouble()!! * it.qty!! }
        val request = HashMap<String, String>()
        request["sub_total"] = String.format(Locale.US, "%.02f", subTotalValue.toDouble())
        request["theme_id"] = getString(R.string.theme_id)
        Utils.showLoadingProgress(this@ActShoppingCart)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActShoppingCart)
                .taxGuest(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            val data = response.body.data
                            response.body.data?.taxInfo?.let { taxAdapter(it) }

                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.TaxInfo,
                                Gson().toJson(response.body.data?.taxInfo)
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.GuestCartSubTotal,
                                currency.plus(Utils.getPrice(data?.originalPrice.toString()))
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.GuestCartTotal,
                                currency.plus(Utils.getPrice(data?.finalPrice.toString()))
                            )

                            _binding.tvSub.text =
                                currency.plus(Utils.getPrice(data?.originalPrice.toString()))
                            _binding.tvTotalPrice.text =
                                currency.plus(Utils.getPrice(data?.finalPrice.toString()))
                            finalTotal = data?.finalPrice.toString()
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.FinalPrice,
                                finalTotal
                            )
                            Log.e("FinalPrice", data?.finalPrice.toString())
                            subTotal = data?.originalPrice.toString()
                        }

                        9 -> {
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                cartListResponse.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActShoppingCart)
                    } else {
                        Utils.errorAlert(
                            this@ActShoppingCart,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActShoppingCart,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActShoppingCart,
                        resources.getString(R.string.something_went_wrong))
                }
            }
        }
    }

    //TODO apply coupon coupon api
    private fun applyCoupon(applyCoupon: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActShoppingCart)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActShoppingCart)
                .applyCoupon(applyCoupon)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val couponResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            _binding.edCouponCode.text.clear()
                            _binding.tvCouponCodeName.show()
                            _binding.tvCouponCodePrice.show()
                            _binding.tvSub.text =
                                currency.plus(Utils.getPrice(couponResponse?.originalPrice.toString()))

                            _binding.tvTotalPrice.text =
                                currency.plus(Utils.getPrice(couponResponse?.discountPrice.toString()))
                            finalTotal = couponResponse?.discountPrice.toString()
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.FinalPrice,
                                finalTotal
                            )

                            if(couponResponse?.couponDiscountType==Constants.FLAT)
                            {
                                _binding.tvCouponCodePrice.text = "-".plus(currency)
                                    .plus(Utils.getPrice(couponResponse.couponDiscountAmount.toString()))
                            }else
                            {
                                _binding.tvCouponCodePrice.text=couponResponse?.couponDiscountAmount.toString().plus("%")
                            }
                            Log.e("couponResponse", couponResponse.toString())

                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.GuestCouponData,
                                Gson().toJson(couponResponse)
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.GuestCartSubTotal,
                                currency.plus(Utils.getPrice(couponResponse?.originalPrice.toString()))
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.GuestCartTotal,
                                currency.plus(Utils.getPrice(couponResponse?.discountPrice.toString()))
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.GuestCartTotal,
                                couponResponse?.discountPrice.toString()
                            )

                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.Coupon_Id,
                                couponResponse?.id.toString()
                            )

                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.Coupon_Name,
                                couponResponse?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.Coupon_Code,
                                couponResponse?.code.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.Coupon_Discount_Type,
                                couponResponse?.couponDiscountType.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.Coupon_Discount_Number,
                                couponResponse?.amount.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.Coupon_Discount_Amount,
                                couponResponse?.couponDiscountAmount.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.Coupon_Final_Amount,
                                couponResponse?.finalPrice.toString()
                            )
                        }
                        0 -> {
                            _binding.tvCouponCodeName.hide()
                            _binding.tvCouponCodePrice.hide()
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                couponResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                couponResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActShoppingCart)
                    } else {
                        Utils.errorAlert(
                            this@ActShoppingCart,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActShoppingCart,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActShoppingCart,
                        resources.getString(R.string.something_went_wrong))
                }
            }
        }
    }

    //TODO set tax data
    private fun taxAdapter(taxList: ArrayList<TaxItem>) {
        _binding.rvTax.layoutManager = managerTax
        taxlistAdapter =
            CartTaxListAdapter(this@ActShoppingCart, taxList) { i: Int, s: String ->
            }
        _binding.rvTax.adapter = taxlistAdapter
    }

    //TODO set offline data
    private fun setupOfflineData() {
        val cartListData = SharePreference.getStringPref(
            this@ActShoppingCart,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListData", cartListData.toString())
        if (cartList == null || cartList.size == 0) {
            _binding.rvShoppingCart.hide()
            _binding.tvTotalQtyCount.hide()
            _binding.clCoupon.hide()
            _binding.clPaymentDetails.hide()
            _binding.tvNoDataFound.show()
            _binding.vieww.show()
        } else {
            _binding.rvShoppingCart.show()
            _binding.tvTotalQtyCount.show()
            _binding.clCoupon.show()
            _binding.clPaymentDetails.show()
            _binding.tvNoDataFound.hide()
            _binding.vieww.hide()
            CategoriesListAdapter(cartList)
            taxGuest(cartList)
            val totalQty = cartList.sumOf { it.qty!! }
            _binding.tvTotalQtyCount.text = totalQty.toString()
        }
    }

    //TODO categories list set
    private fun CategoriesListAdapter(cartList: ArrayList<ProductListItem>) {
        _binding.rvShoppingCart.layoutManager = manager
        cartListAdapter =
            CartListAdapter(this@ActShoppingCart, cartList) { i: Int, s: String ->
                val cartqty = HashMap<String, String>()
                if (s == Constants.ItemDelete) {
                    if (Utils.isLogin(this@ActShoppingCart)) {
                        cartqty["product_id"] = cartList[i].productId.toString()
                        cartqty["user_id"] =
                            SharePreference.getStringPref(
                                this@ActShoppingCart,
                                SharePreference.userId
                            )
                                .toString()
                        cartqty["variant_id"] = cartList[i].variantId.toString()
                        cartqty["quantity_type"] = "remove"
                        cartqty["theme_id"] = getString(R.string.theme_id)
                        cartQtyApi(cartqty, i, "remove")
                    } else {
                        cartList.removeAt(i)
                        cartListAdapter.notifyDataSetChanged()
                        if (cartList.size > 0) {
                            _binding.rvShoppingCart.show()
                            _binding.tvTotalQtyCount.show()
                            _binding.tvNoDataFound.hide()
                            _binding.clCoupon.show()
                            _binding.clPaymentDetails.show()
                        } else {
                            _binding.rvShoppingCart.hide()
                            _binding.tvTotalQtyCount.hide()
                            _binding.clCoupon.hide()
                            _binding.tvNoDataFound.show()

                            _binding.clPaymentDetails.hide()
                        }
                        manageOfflineData(cartList)
                        taxGuest(cartList)
                        val totalQty = cartList.sumOf { it.qty!! }
                        _binding.tvTotalQtyCount.text = totalQty.toString()
                    }
                } else if (s == Constants.IvPlus) {
                    if (Utils.isLogin(this@ActShoppingCart)) {
                        count = cartList[i].qty!! + 1
                        cartqty["product_id"] = cartList[i].productId.toString()
                        cartqty["user_id"] =
                            SharePreference.getStringPref(
                                this@ActShoppingCart,
                                SharePreference.userId
                            ).toString()
                        cartqty["variant_id"] = cartList[i].variantId.toString()
                        cartqty["quantity_type"] = "increase"
                        cartqty["theme_id"] = getString(R.string.theme_id)
                        cartQtyApi(cartqty, i, "increase")
                    } else {
                        cartList[i].qty = cartList[i].qty?.plus(1)
                        cartListAdapter.notifyDataSetChanged()
                        manageOfflineData(cartList)
                        taxGuest(cartList)
                        val totalQty = cartList.sumOf { it.qty!! }
                        _binding.tvTotalQtyCount.text = totalQty.toString()
                    }

                } else if (s == Constants.IvMinus) {
                    if (cartList[i].qty!!.toInt() > 1) {
                        if (Utils.isLogin(this@ActShoppingCart)) {
                            count = cartList[i].qty!! - 1
                            Utils.getLog("Qty>>", cartList[i].qty.toString())
                            cartqty["product_id"] = cartList[i].productId.toString()
                            cartqty["user_id"] =
                                SharePreference.getStringPref(
                                    this@ActShoppingCart,
                                    SharePreference.userId
                                ).toString()
                            cartqty["variant_id"] = cartList[i].variantId.toString()
                            cartqty["quantity_type"] = "decrease"
                            cartqty["theme_id"] = getString(R.string.theme_id)
                            cartQtyApi(cartqty, i, "decrease")
                        } else {
                            cartList[i].qty = cartList[i].qty?.minus(1)
                            cartListAdapter.notifyDataSetChanged()
                            manageOfflineData(cartList)
                            taxGuest(cartList)
                            val totalQty = cartList.sumOf { it.qty!! }
                            _binding.tvTotalQtyCount.text = totalQty.toString()
                        }
                    } else {
                        Utils.getLog("Qty1>>", cartList[i].qty.toString())
                    }
                }
            }
        _binding.rvShoppingCart.adapter = cartListAdapter
    }

    //TODO guest user offline data
    private fun manageOfflineData(cartItemList: ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActShoppingCart,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActShoppingCart,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    //TODO cart qty api
    private fun cartQtyApi(cartqty: HashMap<String, String>, position: Int, type: String) {
        Utils.showLoadingProgress(this@ActShoppingCart)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActShoppingCart)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActShoppingCart,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            when (type) {
                                "increase" -> {
                                    cartList[position].qty = count
                                    val totalQty = cartList.sumOf { it.qty!! }
                                    _binding.tvTotalQtyCount.text = totalQty.toString()
                                    cartListAdapter.notifyDataSetChanged()
                                }
                                "remove" -> {
                                    cartList.removeAt(position)
                                    cartListAdapter.notifyDataSetChanged()
                                    if (cartList.size > 0) {
                                        _binding.rvShoppingCart.show()
                                        _binding.tvTotalQtyCount.show()
                                        _binding.clCoupon.show()
                                        _binding.tvNoDataFound.hide()
                                        _binding.clPaymentDetails.show()
                                    } else {
                                        _binding.rvShoppingCart.hide()
                                        _binding.tvTotalQtyCount.hide()
                                        _binding.clCoupon.hide()
                                        _binding.tvNoDataFound.show()
                                        _binding.clPaymentDetails.hide()
                                    }
                                    val totalQty = cartList.sumOf { it.qty!! }
                                    _binding.tvTotalQtyCount.text = totalQty.toString()
                                }
                                "decrease" -> {
                                    cartList[position].qty = count
                                    val totalQty = cartList.sumOf { it.qty!! }
                                    _binding.tvTotalQtyCount.text = totalQty.toString()
                                    cartListAdapter.notifyDataSetChanged()
                                }
                            }
                            getCartList()
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                cartQtyResponse?.message.toString()
                            )
                        }

                        /* cartList[position].qty = count
                                   val totalQty = cartList.sumOf { it.qty!! }
                                   _binding.tvTotalQtyCount.text = totalQty.toString()
                                   cartListAdapter.notifyDataSetChanged()*/
                        9 -> {
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActShoppingCart)
                    } else {
                        Utils.errorAlert(
                            this@ActShoppingCart,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActShoppingCart,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActShoppingCart, resources.getString(R.string.theme_id))
                }
            }
        }
    }

    //TODO cart list api
    private fun getCartList() {
        Utils.showLoadingProgress(this@ActShoppingCart)
        val cartListMap = HashMap<String, String>()
        cartListMap["user_id"] =
            SharePreference.getStringPref(this@ActShoppingCart, SharePreference.userId)
                .toString()
        cartListMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActShoppingCart)
                .cartList(cartListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.productList?.size ?: 0) > 0) {
                                _binding.rvShoppingCart.show()
                                _binding.tvTotalQtyCount.show()
                                _binding.vieww.hide()
                                _binding.tvNoDataFound.hide()
                                _binding.clCoupon.show()
                                _binding.clPaymentDetails.show()
                                cartList = ArrayList()
                                taxlist = ArrayList()
                                response.body.data?.productList?.let { cartList.addAll(it) }
                                cartListResponse?.taxInfo?.let {
                                    taxlist.addAll(it)
                                }
                                CategoriesListAdapter(cartList)
                                if (taxlist.size == 2) {
                                    _binding.rvTax.isNestedScrollingEnabled = false;
                                }
                                taxAdapter(taxlist)
                                val totalQty = cartList.sumOf { it.qty!! }
                                _binding.tvTotalQtyCount.text = totalQty.toString()
                                cartListAdapter.notifyDataSetChanged()
                                taxlistAdapter.notifyDataSetChanged()
                            } else {
                                _binding.rvShoppingCart.hide()
                                _binding.tvTotalQtyCount.hide()
                                _binding.vieww.show()
                                _binding.clCoupon.hide()
                                _binding.clPaymentDetails.hide()
                                _binding.tvNoDataFound.show()
                            }


                            _binding.tvSub.text =
                                currency.plus(Utils.getPrice(cartListResponse?.subTotal.toString()))
                            _binding.tvTotalPrice.text =
                                currency.plus(Utils.getPrice(cartListResponse?.finalPrice.toString()))
                            finalTotal = cartListResponse?.finalPrice.toString()
                            SharePreference.setStringPref(
                                this@ActShoppingCart,
                                SharePreference.FinalPrice,
                                finalTotal
                            )
                            subTotal = cartListResponse?.subTotal.toString()
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                cartListResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActShoppingCart,
                                cartListResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActShoppingCart)
                    } else {
                        Utils.errorAlert(
                            this@ActShoppingCart,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActShoppingCart,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActShoppingCart, resources.getString(R.string.something_went_wrong))
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()

        if (Utils.isLogin(this@ActShoppingCart)) {
            getCartList()
        } else {
            _binding.vieww.hide()
            setupOfflineData()
        }
        _binding.tvCouponCodeName.hide()
        _binding.tvCouponCodePrice.hide()
    }

}