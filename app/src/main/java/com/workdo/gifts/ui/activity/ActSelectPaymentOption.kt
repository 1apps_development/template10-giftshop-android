package com.workdo.gifts.ui.activity

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.gifts.R
import com.workdo.gifts.adapter.PaymentAdapter
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.base.BaseActivity
import com.workdo.gifts.databinding.ActSelectPaymentOptionBinding
import com.workdo.gifts.model.PaymentData
import com.workdo.gifts.remote.NetworkResponse
import com.workdo.gifts.ui.authentication.ActWelCome
import com.workdo.gifts.utils.ExtensionFunctions.hide
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils
import kotlinx.coroutines.launch


class ActSelectPaymentOption : BaseActivity() {
    private lateinit var _binding: ActSelectPaymentOptionBinding
    private var paymentList = ArrayList<PaymentData>()
    private lateinit var paymentAdapter: PaymentAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""
    var paymentName = ""
    var stripeKey = ""

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSelectPaymentOptionBinding.inflate(layoutInflater)
        init()
    }

    private fun init(){
        Log.e("stripeKey", stripeKey)
        _binding.ivBack.setOnClickListener { finish() }
        _binding.chbTermsCondition.isChecked = false
        _binding.chbTermsCondition.setOnClickListener {
            _binding.btnContinue.isEnabled = _binding.chbTermsCondition.isChecked
        }

        _binding.btnContinue.setOnClickListener {
            if (_binding.chbTermsCondition.isChecked) {

                comment = _binding.edNote.text.toString()
                SharePreference.setStringPref(
                    this@ActSelectPaymentOption,
                    SharePreference.Payment_Comment,
                    comment.toString()
                )
                openActivity(ActOrderConfirm::class.java)
            }
        }
        manager = GridLayoutManager(this@ActSelectPaymentOption, 1, GridLayoutManager.VERTICAL, false)
        _binding.tvTerms.setOnClickListener {
            val terms =
                SharePreference.getStringPref(this@ActSelectPaymentOption, SharePreference.Terms).toString()
            val uri: Uri =
                Uri.parse(terms)

            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    //TODO payment list api
    private fun callPaymentList() {
        Utils.showLoadingProgress(this@ActSelectPaymentOption)
        val paymentListMap = HashMap<String, String>()
        paymentListMap["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSelectPaymentOption)
                .paymentList(paymentListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvPayment.show()
                                _binding.vieww.hide()

                                runOnUiThread {
                                    paymentListResponse.data?.let {
                                        paymentList.addAll(it)
                                    }
                                }
                                paymentList.removeAll {
                                    it.status == "off"
                                }
                                PaymentListAdapter(paymentList)
                            } else {
                                _binding.rvPayment.hide()
                                _binding.vieww.show()
                            }
                            paymentAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActSelectPaymentOption,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSelectPaymentOption,
                                response.body.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActSelectPaymentOption)
                    } else {
                        Utils.errorAlert(
                            this@ActSelectPaymentOption,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSelectPaymentOption,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActSelectPaymentOption,
                        resources.getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }


    //TODO payment list data set
    private fun PaymentListAdapter(paymentList: ArrayList<PaymentData>) {
        _binding.rvPayment.layoutManager = manager
        paymentAdapter =
            PaymentAdapter(this@ActSelectPaymentOption, paymentList) { i: Int, s: String ->
                if (paymentList[i].isSelect == true) {
                    paymentName = paymentList[i].nameString.toString()

                    SharePreference.setStringPref(
                        this@ActSelectPaymentOption,
                        SharePreference.Payment_Type,
                        paymentName
                    )

                    SharePreference.setStringPref(
                        this@ActSelectPaymentOption,
                        SharePreference.PaymentImage,
                        paymentList[i].image.toString()
                    )
                } else {

                }
            }
        _binding.rvPayment.adapter = paymentAdapter
    }


    override fun onResume() {
        super.onResume()
        paymentList.clear()
        callPaymentList()
    }

}