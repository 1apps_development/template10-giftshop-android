package com.workdo.gifts.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.databinding.CellProductBinding
import com.workdo.gifts.model.FeaturedProductsSub
import com.workdo.gifts.utils.Constants
import com.workdo.gifts.utils.ExtensionFunctions.capitalized
import com.workdo.gifts.utils.ExtensionFunctions.hide
import com.workdo.gifts.utils.ExtensionFunctions.invisible
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils

class SearchListAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = CellProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(providerList[position], context, position, itemClick)

    }

    override fun getItemCount(): Int {
        return providerList.size
    }

    inner class ViewHolder(private val binding: CellProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivProduct)
            binding.tvProductSubTitle.text = data.name.toString()
            binding.tvProductPrice.text =Utils.getPrice(data.finalPrice.toString())
            binding.tvProductTitle.text = data.tagApi?.capitalized()
            binding.tvCurrency.text = currency

           // binding.tvTag.text = data.tagApi
            if (data.inWhishlist == true) {
                binding.ivWishList.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources,R.drawable.ic_wishlist,
                        null
                    )
            } else {
                binding.ivWishList.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources,R.drawable.ic_diswishlist,
                        null
                    )
            }


            if (Utils.isLogin(context)) {
                binding.ivWishList.show()
            } else {
                binding.ivWishList.invisible()

            }
            binding.btnCheckMore.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            binding.ivWishList.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

}