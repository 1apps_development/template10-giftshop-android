package com.workdo.gifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.gifts.R
import com.workdo.gifts.databinding.CellOrderHistoryBinding
import com.workdo.gifts.model.OrderListData
import com.workdo.gifts.utils.Constants
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils

class OrderHistorylistAdapter(
    private val context: Activity,
    private val orderlist: ArrayList<OrderListData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<OrderHistorylistAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellOrderHistoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: OrderListData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            binding.tvDate.text =
                context.getString(R.string.date).plus(" ").plus(Utils.getDate(data.date.toString()))
            binding.tvOrderId.text = "#".plus(data.productOrderId)
            binding.tvFeaturedProductPrice.text =
                currency.plus(Utils.getPrice(data.amount.toString()))
            binding.tvOrderStatus.text = context.getString(R.string.statue_).plus(" ").plus(data.deliveredStatusString)
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellOrderHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(orderlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return orderlist.size
    }
}