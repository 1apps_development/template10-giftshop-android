package com.workdo.gifts.adapter.variant

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.gifts.R
import com.workdo.gifts.databinding.CellSizeBinding
import com.workdo.gifts.model.VariantDataItem

class VariantCollectionList(
    val itemList:ArrayList<VariantDataItem>,
    val context: Activity, val variantItemClick: (VariantDataItem) -> Unit
):RecyclerView.Adapter<VariantCollectionList.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =CellSizeBinding.inflate(LayoutInflater.from(parent.context),parent,false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(itemList[position],context,variantItemClick,position)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

  inner  class ViewHolder(val itemBinding: CellSizeBinding,) : RecyclerView.ViewHolder(itemBinding.root) {


        fun bindItems(data:VariantDataItem, context:Activity, variantItemClick: (VariantDataItem) -> Unit,position: Int)
        {

            if (data.isSelect == true) {
                itemBinding.card.background =
                    ResourcesCompat.getDrawable(itemView.context.resources, R.drawable.bg_blue_circle, null)
                itemBinding.tvsize.setTextColor(
                    AppCompatResources.getColorStateList(
                        itemView.context, R.color.pink
                    )
                )
            } else {
                itemBinding.card.background = ResourcesCompat.getDrawable(context.resources, R.drawable.bg_white_11, null)
                itemBinding.tvsize.setTextColor(
                    AppCompatResources.getColorStateList(
                        context, R.color.dark_blue
                    )
                )
            }
            itemBinding.tvsize.text = data.name
            itemView.setOnClickListener {
                for(i in 0 until itemList.size)
                {
                    itemList[i].isSelect=false
                }
                itemList[position].isSelect=true
                notifyDataSetChanged()
                variantItemClick(data)
            }
        }

    }
}