package com.workdo.gifts.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.databinding.CellRattingBinding
import com.workdo.gifts.model.ProductReview
import com.workdo.gifts.utils.SharePreference

class RattingAdapter(
    var context: Activity,
    private val rattingList: ArrayList<ProductReview>,
    private val onItemClick: (String, String) -> Unit
) : RecyclerView.Adapter<RattingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =CellRattingBinding.inflate(LayoutInflater.from(parent.context),parent,false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(rattingList[position])

    }

    override fun getItemCount(): Int {
        return rattingList.size
    }

    class ViewHolder(val binding: CellRattingBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bindItems(data :ProductReview)= with(binding)
        {
            val profile=SharePreference.getStringPref(itemView.context,SharePreference.PaymentUrl)
            Log.e("imageurl",profile.plus(data.userImage))

            Glide.with(itemView.context).load(profile.plus(data.userImage)).into(binding.ivProfile)
          //  Glide.with(itemView.context).load(ApiClient.ImageURL.BASE_URL.plus(data.userImage)).into(binding.ivUserImage)
            binding.tvReviewTitle.text=data.title.toString()
          //  binding.tvSubTitle.text=data.subTitle.toString()
            binding.tvReviewDescription.text=data.review.toString()
            binding.tvClientName.text=data.userName.toString()
            binding.tvUserRatting.text = data.rating?.toFloat().toString()
            binding.ivratting.rating=data.rating?.toFloat()?:0.0f
        }

    }

    private fun onFilterClick(id: String, name: String) {
        onItemClick.invoke(id, name)
    }
}