package com.workdo.gifts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.databinding.CellCategoryBinding
import com.workdo.gifts.databinding.CellProductlistingBinding
import com.workdo.gifts.model.HomeCategoriesItem
import com.workdo.gifts.utils.Constants

class CategorysProductAdapter (
    private val context: Activity,
    private val categoryList: ArrayList<HomeCategoriesItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CategorysProductAdapter.AllCateViewHolder>() {

    inner class AllCateViewHolder(private val binding: CellProductlistingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: HomeCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.image)).into(binding.ivCart)
            binding.tvProductName.text = data.name.toString()

            // binding.tvCount.text =context.getString(R.string.categoriess)
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllCateViewHolder {
        val view = CellProductlistingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AllCateViewHolder(view)
    }

    override fun onBindViewHolder(holder: AllCateViewHolder, position: Int) {
        holder.bind(categoryList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }
}