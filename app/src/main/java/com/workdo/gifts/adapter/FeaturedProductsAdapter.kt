package com.workdo.gifts.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.databinding.CellHomeProductBinding
import com.workdo.gifts.model.FeaturedProductsSub
import com.workdo.gifts.utils.Constants
import com.workdo.gifts.utils.ExtensionFunctions.capitalized
import com.workdo.gifts.utils.ExtensionFunctions.hide
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils

class FeaturedProductsAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit,
) : RecyclerView.Adapter<FeaturedProductsAdapter.FeaturedViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    inner class FeaturedViewHolder(private val binding: CellHomeProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit,
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivProduct)
            binding.tvProductSubTitle.text = data.name.toString()
            binding.tvProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            binding.tvCurrency.text = currency
            binding.tvProductTitle.text = data.tagApi?.capitalized()

            if (data.inWhishlist == true) {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }

          /*  if (data?.productStock == 0) {*//*
                            _binding.btnAddtoCard.isEnabled = false
                            _binding.btnAddtoCard.isClickable = false*//*
                *//*Utils.errorAlert(
                    this@ActProductDetails,
                    resources.getString(R.string.out_of_stock)
                )*//*
                binding.btnCheckMore.text = context.resources.getString(R.string.notify_me)

            } else {
                *//*_binding.btnAddtoCard.isEnabled = true
                _binding.btnAddtoCard.isClickable = true*//*

                binding.btnCheckMore.text = context.resources.getString(R.string.add_to_cart)
            }*/

            if (Utils.isLogin(context)) {
                binding.ivWishList.show()
            } else {
                binding.ivWishList.hide()

            }
            binding.ivWishList.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.btnCheckMore.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
        val view =
            CellHomeProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FeaturedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}