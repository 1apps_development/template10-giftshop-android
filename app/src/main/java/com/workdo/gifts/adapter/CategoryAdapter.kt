package com.workdo.gifts.adapter

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.gifts.model.CategoryDataItem
import com.workdo.gifts.R
import com.workdo.gifts.api.ApiClient
import com.workdo.gifts.databinding.CellCategoryssBinding
import com.workdo.gifts.ui.activity.ActCategoryProduct
import com.workdo.gifts.utils.Constants
import com.workdo.gifts.utils.ExtensionFunctions.invisible
import com.workdo.gifts.utils.ExtensionFunctions.show
import com.workdo.gifts.utils.SharePreference
import com.workdo.gifts.utils.Utils

class CategoryAdapter (
    private val context: Activity,
    private val providerList: ArrayList<CategoryDataItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.BestSellerViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    inner class BestSellerViewHolder(private val binding: CellCategoryssBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: CategoryDataItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.imagePath))
                .into(binding.ivImg)

            binding.tvCatTitle.text=data.name.toString()

            binding.btnCheckMore.setOnClickListener {
                val intent = Intent(context, ActCategoryProduct::class.java)
                intent.putExtra("maincategory_id",data.maincategoryId.toString())
                context.startActivity(intent)
            }

            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestSellerViewHolder {
        val view =
            CellCategoryssBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return BestSellerViewHolder(view)
    }

    override fun onBindViewHolder(holder: BestSellerViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}