package com.workdo.gifts.utils;

import com.workdo.gifts.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
