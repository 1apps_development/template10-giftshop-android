package com.workdo.gifts.utils;

import com.workdo.gifts.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
