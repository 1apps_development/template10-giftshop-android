package com.workdo.gifts.utils;

import com.workdo.gifts.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
