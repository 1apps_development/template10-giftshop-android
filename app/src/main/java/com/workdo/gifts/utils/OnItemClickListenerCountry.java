package com.workdo.gifts.utils;

import com.workdo.gifts.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
