package com.workdo.gifts.utils;

import com.workdo.gifts.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
