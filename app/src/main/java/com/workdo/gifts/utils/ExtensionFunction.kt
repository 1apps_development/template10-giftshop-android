package com.workdo.gifts.utils

import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.util.*

object ExtensionFunctions {

    fun View.show(){
        this.visibility = View.VISIBLE
    }

    fun View.hide(){
        this.visibility = View.GONE
    }
    fun View.invisible(){
        this.visibility = View.INVISIBLE
    }

    fun isValid( lastClickTime:Long): Boolean {

        val current = System.currentTimeMillis()
        val def: Long = current - lastClickTime
        return def > 1000
    }

    fun Activity.hideKeyboard() {
        val imm: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val view = currentFocus ?: View(this)
        imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    fun String.capitalized(): String {
        return this.replaceFirstChar {
            if (it.isLowerCase())
                it.titlecase(Locale.getDefault())
            else it.toString()
        }
    }

    fun View.isDisable(){
        this.isClickable=false
        this.isEnabled=false
    }

    fun View.isEnable(){
        this.isClickable=true
        this.isEnabled=true
    }

}