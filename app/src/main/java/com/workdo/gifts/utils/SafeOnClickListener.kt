package com.workdo.gifts.utils

import android.util.Log
import android.view.View

abstract class SafeOnClickListener : View.OnClickListener {

    private var lastClickMs: Long = 0
    private val TAG = "SafeOnClickListener"
    private val TOO_SOON_DURATION_MS: Long = 700

    /**
     * Override onOneClick() instead.
     */
    override fun onClick(v: View?) {
        val nowMs = System.currentTimeMillis()
        if (lastClickMs != 0L && nowMs - lastClickMs < TOO_SOON_DURATION_MS) {
            Log.d(TAG, "onClick: too soon")
            return
        }
        Log.d(TAG, "onClick: Ok")
        lastClickMs = nowMs
        onOneClick(v)
    }

    fun isInteractionTooSoon(): Boolean {
        var retVal = false
        val nowMs = System.currentTimeMillis()
        if (lastClickMs != 0L && nowMs - lastClickMs < TOO_SOON_DURATION_MS) {
            //TOO SOON
            retVal = true
        }
        return retVal
    }


    abstract fun onOneClick(v: View?)
}