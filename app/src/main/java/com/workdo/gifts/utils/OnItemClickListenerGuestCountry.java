package com.workdo.gifts.utils;

import com.workdo.gifts.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
