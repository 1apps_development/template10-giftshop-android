package com.workdo.gifts.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class LandingPageData(
	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null
)

data class HomepageHeader(

	@field:SerializedName("homepage-header-bg-img")
	val homepageHeaderBgImg: ArrayList<String>? = null,

	@field:SerializedName("homepage-header-heading")
	val homepageHeaderHeading: String? = null,

	@field:SerializedName("homepage-header-sub-text")
	val homepageHeaderSubText: String? = null,

	@field:SerializedName("homepage-header-title-text")
	val homepageHeaderTitleText: String? = null,

	@field:SerializedName("homepage-header-btn-text")
	val homepageHeaderBtnText: String? = null
)

data class HomepageNewsletter(

	@field:SerializedName("homepage-newsletter-sub-text")
	val homepageNewsletterSubText: String? = null,

	@field:SerializedName("homepage-newsletter-description")
	val homepageNewsletterDescription: String? = null,

	@field:SerializedName("homepage-newsletter-title-text")
	val homepageNewsletterTitleText: String? = null
)
data class HomePageHeaderBg(
	@field:SerializedName("homepage-header-bg-img")
	val homePageHeaderBgImg: String? = null
)

data class HomepageCategories(

	@field:SerializedName("homepage-categories-title-text")
	val homepageCategoriesTitleText: String? = null
)

data class HomepageProducts(

	@field:SerializedName("homepage-products-title-text")
	val homepageProductsTitleText: String? = null
)

data class ThemJson(

	@field:SerializedName("homepage-header")
	val homepageHeader: HomepageHeader? = null,

	@field:SerializedName("homepage-bestproducts")
	val homepageBestproducts: HomepageBestproducts? = null,

	@field:SerializedName("homepage-review")
	val homepageReview: HomepageReview? = null,

	@field:SerializedName("homepage-newsletter")
	val homepageNewsletter: HomepageNewsletter? = null,

	@field:SerializedName("homepage-products")
	val homepageProducts: HomepageProducts? = null,

	@field:SerializedName("homepage-categories")
	val homepageCategories: HomepageCategories? = null
)

data class HomepageReview(

	@field:SerializedName("homepage-review-title-text")
	val homepageReviewTitleText: String? = null,

	@field:SerializedName("homepage-review-heading")
	val homepageReviewHeading: String? = null,

	@field:SerializedName("homepage-review-sub-text")
	val homepageReviewSubText: String? = null
)

data class HomepageBestproducts(

	@field:SerializedName("homepage-bestproducts-title-text")
	val homepageBestproductsTitleText: String? = null,

	@field:SerializedName("homepage-bestproducts-btn-text")
	val homepageBestproductsBtnText: String? = null
)
