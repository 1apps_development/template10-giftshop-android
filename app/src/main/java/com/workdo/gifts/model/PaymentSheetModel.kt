package com.workdo.gifts.model

import com.google.gson.annotations.SerializedName

data class PaymentSheetModel(

	@field:SerializedName("clientSecret")
	val clientSecret: String? = null
)
