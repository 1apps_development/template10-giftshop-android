package com.workdo.gifts.model

import com.google.gson.annotations.SerializedName

data class BaseUrlResponse(

	@field:SerializedName("data")
	val data: UrlData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class UrlData(

	@field:SerializedName("payment_url")
	val paymentUrl: String? = null,

	@field:SerializedName("image_url")
	val imageUrl: String? = null,

	@field:SerializedName("base_url")
	val baseUrl: String? = null
)
